require 'tapyrus/version'

module Tapyrus
  module Rpc
    autoload :TapyrusCoreClient, 'tapyrus/rpc/tapyrus_core_client.rb'
  end
end
